<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="idPost")
     */
    private $idPost;

    /**
     * @ORM\Column(type="string", length=255, name="sTitle")
     */
    private $sTitle;

    /**
     * @ORM\Column(type="string", length=255, name="sSubTitle")
     */
    private $sSubTitle;

    /**
     * @ORM\Column(type="string", length=255, name="sCategory")
     */
    private $sCategory;

    public function getId(): ?int
    {
        return $this->idPost;
    }

    public function getSTitle(): ?string
    {
        return $this->sTitle;
    }

    public function setSTitle(string $sTitle): self
    {
        $this->sTitle = $sTitle;

        return $this;
    }

    public function getSSubTitle(): ?string
    {
        return $this->sSubTitle;
    }

    public function setSSubTitle(string $sSubTitle): self
    {
        $this->sSubTitle = $sSubTitle;

        return $this;
    }

    public function getSCategory(): ?string
    {
        return $this->sCategory;
    }

    public function setSCategory(string $sCategory): self
    {
        $this->sCategory = $sCategory;

        return $this;
    }
}
