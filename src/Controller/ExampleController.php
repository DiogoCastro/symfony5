<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Task;
use App\Form\ExampleFormType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;

class ExampleController extends AbstractController
{

    /**
     * List the rewards of the specified user.
     *
     * This call takes into account all confirmed awards, but not pending or refused awards.
     *
     * @Route("/api/{user}/rewards", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Task::class, groups={"full"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     type="string",
     *     description="The field used to order rewards"
     * )
     * @SWG\Tag(name="rewards")
     * @Security(name="Bearer")
     */
    public function fetchUserRewardsAction(Task $task)
    {
        //return $this->json()
        // ... test to later use in a frontend app in angular
    }

    /**
     * @Route("/", name="example")
     * @throws Exception
     */
    public function index()
    {
        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findAll();

        if (!$post) {
            throw $this->createNotFoundException('No post found');
        }

        return $this->render('example/index.html.twig', [
            'controller_name' => 'ExampleController',
            'extra_var' => $this->sopas("test extra var"),
            'form' => $this->new(),
            'post' => $post
        ]);
    }

    /**
     * @param string $asd
     * @return string
     */
    protected function sopas(string $asd = "")
    {
        return $asd;
    }

    /**
     * @throws Exception
     */
    protected function new()
    {
        // creates a task object and initializes some data for this example
        $task = new Task();
        $task->setTask('Write a blog to insert in posts');
        $task->setDueDate(new \DateTime('tomorrow'));

        $form = $this->createForm(ExampleFormType::class, $task);

        return $form->createView();
    }
}
