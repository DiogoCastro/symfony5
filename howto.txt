you need composer and yarn

composer install

//to install all yarn dependencies
yarn install

//to build the public/build/*
yarn encore dev

//same as previous cmd but as a watcher
//yarn encore dev --watch